import React from "react";
import { StyleSheet } from "react-native";
import { View } from "react-native-paper";
import { theme } from "../core/theme";

export default function Container(props) {
  return <View style={styles.container} {...props} />;
}

const styles = StyleSheet.create({
  container: {
    fontSize: 21,
    color: theme.colors.primary,
    fontWeight: "bold",
    paddingVertical: 12,
  },
});
