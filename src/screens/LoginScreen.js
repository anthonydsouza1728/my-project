import React, { useState } from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";
import { Text, TextInput as RightIcon } from "react-native-paper";
import Background from "../components/Background";
import Header from "../components/Header";
import Button from "../components/Button";
import TextInput from "../components/TextInput";
import { theme } from "../core/theme";
import { emailValidator } from "../helpers/emailValidator";
import { passwordValidator } from "../helpers/passwordValidator";
import { Octicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });

  const onLoginPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }
    navigation.reset({
      index: 0,
      routes: [{ name: "Dashboard" }],
    });
  };

  return (
    <Background>
      <Header>Welcome Back!</Header>
      <Text style={styles.headertext}>
        Let's get you signed in and we will make your work life smoother
        together.
      </Text>
      <View
        style={{
          backgroundColor: theme.colors.surface,
          paddingTop: 10,
          paddingRight: 30,
          paddingLeft: 30,
          paddingBottom: "auto",
          borderTopRightRadius: 10,
          borderTopLeftRadius: 10,
        }}
      >
        <Text style={styles.innertext}>
          Ensure that your account is associated with your company's email
          address to access our application
        </Text>
        <TextInput
          name={<MaterialIcons name="email" size={24} color="black" />}
          label="example@gmail.com"
          returnKeyType="next"
          value={email.value}
          onChangeText={(text) => setEmail({ value: text, error: "" })}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />

        <TextInput
          name={<MaterialCommunityIcons name="lock" size={24} color="black" />}
          label="********"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({ value: text, error: "" })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
          right={<RightIcon.Icon name="eye" />}
        />

        <View style={styles.forgotPassword}>
          <TouchableOpacity
            onPress={() => navigation.navigate("ResetPasswordScreen")}
          >
            <Text style={styles.forgot}>Forgot your password ?</Text>
          </TouchableOpacity>
        </View>
        <Button
          mode="contained"
          onPress={onLoginPressed}
          style={{
            alignItems: "center",
          }}
        >
          Sign in
        </Button>

        {/* Line */}
        <View style={styles.lineStyle}>
          <View
            style={{
              flex: 1,
              height: 1,
              backgroundColor: theme.colors.primary,
            }}
          />
          <View>
            <Text style={{ width: 70, textAlign: "center" }}>or continue</Text>
          </View>
          <View
            style={{
              flex: 1,
              height: 1,
              backgroundColor: theme.colors.primary,
            }}
          />
        </View>

        <Button
          style={{
            backgroundColor: theme.colors.onCompanyButton,
            alignItems: "center",
          }}
          mode="contained"
          onPress={onLoginPressed}
        >
          Sign in with Company's URL
        </Button>
        <Text style={styles.underText}>
          If you encounter issues please contact your company's HR department
          for assurance
        </Text>
      </View>
    </Background>
  );
}

const styles = StyleSheet.create({
  forgotPassword: {
    width: "100%",
    alignItems: "center",
    marginBottom: 10,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    color: theme.colors.secondary,
  },
  headertext: {
    textAlign: "center",
    color: theme.colors.surface,
    marginBottom: 30,
  },
  innertext: {
    color: theme.colors.secondary,
    margin: 10,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
  lineStyle: {
    flexDirection: "row",
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    alignItems: "center",
  },
  underText: {
    marginTop: 10,
    padding: 20,
  },
});
